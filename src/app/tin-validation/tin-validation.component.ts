import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageServiceService } from '../service/storage-service.service';
import { ApiServiceService } from '../service/api-service.service';
import { toast } from 'angular2-materialize';

@Component({
  selector: 'app-tin-validation',
  templateUrl: './tin-validation.component.html',
  styleUrls: ['./tin-validation.component.css']
})
export class TinValidationComponent implements OnInit {
  tin:any;
  getInfo:any;
  constructor(private api_service:ApiServiceService,private route: ActivatedRoute, private router: Router, private storage_service: StorageServiceService) { 
    if (!this.getInfo) {
      this.getInfo = {
        state:''
      }
      
    } 
  }

  ngOnInit() {

    // var key = "custObj";
    // var userInfo = JSON.parse(this.storage_service.getInfo(key));
    // var i = 60;

   

    // var key1 = "otpVerify";
    // var userInfo1 = JSON.parse(this.storage_service.getInfo(key1));


    // if(!userInfo1){
    //   this.router.navigate(['/welcome']);
    // }
    // else if((userInfo1 != null)&&(userInfo != null)){
    //   this.tin = userInfo.tin;
    //   this.getInfo.state = 'good';
    //   this.storage_service.saveInfo('tinValid', JSON.stringify(this.getInfo));
  
      
    // }
  }

  back(){

    this.router.navigate(['/welcome']);
  }

  correct(){

    this.router.navigate(['/congrats']);
  }

}
