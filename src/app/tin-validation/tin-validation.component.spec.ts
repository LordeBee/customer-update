import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TinValidationComponent } from './tin-validation.component';

describe('TinValidationComponent', () => {
  let component: TinValidationComponent;
  let fixture: ComponentFixture<TinValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TinValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TinValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
