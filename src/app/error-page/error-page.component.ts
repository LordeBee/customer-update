import { Component, OnInit } from '@angular/core';
import { StorageServiceService } from '../service/storage-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {

  constructor(private storage_service: StorageServiceService, private router: Router) { }

  ngOnInit() {

    

    
  }

  continue(){
    this.router.navigate(['/user-validation']);

  }

}
