import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageServiceService } from '../service/storage-service.service';
import { ApiServiceService } from '../service/api-service.service';
import { toast } from 'angular2-materialize';
import { CookieService } from 'ngx-cookie-service';

declare var $:any;

@Component({
  selector: 'app-otp-verify',
  templateUrl: './otp-verify.component.html',
  styleUrls: ['./otp-verify.component.css']
})
export class OtpVerifyComponent implements OnInit {

  phone:any;
  resp:any;
  code:any;
  tin:any;
  formData:any;
  showphone:any;
  cif:any;
  getInfo:any;
  formInfo:any;
  otpObj:any;
  response:any;
  loader=false;
  start:any;
  
  


  constructor(private cookieService: CookieService,private api_service:ApiServiceService,private route: ActivatedRoute, private router: Router, private storage_service: StorageServiceService) { 
    if (!this.formData) {
      this.formData = {
        c1:'',
        c2:'',
        c3:'',
        c4:'',
        c5:''
      }
      
    } 

    if (!this.getInfo) {
      this.getInfo = {
        state:''
      }
      
    } 

    if (!this.formInfo) {
      this.formInfo = {
        hostHeaderInfo:{
          requestId:"1",
          ipAddress:"127.0.0.1",
          sourceChannelId:"MOBILE APP"
       },
        otp:"",
        cif:""
      }
      
    } 

    if (!this.otpObj) {
      this.otpObj = {
        hostHeaderInfo:{
          requestId:"1",
          ipAddress:"127.0.0.1",
          sourceChannelId:"web"
       },
      accountNumber:""
      }
      
    }
  }  

  ngOnInit() {
    // var key = "custObj";
    //console.log(this.storage_service.loggedIn());
    
    var key = 'userObj';
    var userInfo = JSON.parse(this.storage_service.getInfo(key));
    if (userInfo != null) {
       this.formInfo.cif = userInfo.cif;
       this.phone = userInfo.phone;
    }  
    else{
      this.router.navigate(['/welcome']);
    } 
   
    // console.log(UserInfo);
    var i = 60;


 
      this.start = 1
  
      this.showphone = this.phone;
 
      
    
    $(".now").hide();
    $(".loading").hide();

    $(".inputs").keyup(function () {
      if (this.value.length == this.maxLength) {
        $(this).next('.inputs').focus();
      }
  });

  $('.inputs').on('keyup', function(e) {

      if( e.which == 8 || e.which == 46 ){
        // alert("fdf");

        $(this).prev('.inputs').focus();
      }

    
    
  
  });

    function onTimer() {
      if (i==60) {
        document.getElementById('mycounter').innerHTML = '1 : 00';
      }
      else if(i<=9){
        document.getElementById('mycounter').innerHTML = '00 : 0' + i;
      }
      else if(i<=59){
        document.getElementById('mycounter').innerHTML = '00 : ' + i;
      }

     
       
      i--;
      if (i < 0) {
        $(".now").show();
        $(".h4").hide();
        $("#mycounter").hide();
      }
      else {
        setTimeout(onTimer, 1000);
      }
    }

    if(this.start==1){
      onTimer();
    }


    

  }

  resend(){


    var i = 60;
    $(".now").hide();
    $(".h4").show();
    $("#mycounter").show();

    function onTimer() {
      if (i==60) {
        document.getElementById('mycounter').innerHTML = '1 : 00';
      }
      else if(i<=9){
        document.getElementById('mycounter').innerHTML = '00 : 0' + i;
      }
      else if(i<=59){
        document.getElementById('mycounter').innerHTML = '00 : ' + i;
      }

     
       
      i--;
      if (i < 0) {
        $(".now").show();
        $(".h4").hide();
        $("#mycounter").hide();
      }
      else {
        setTimeout(onTimer, 1000);
      }
    }
    this.start = 1;

    if(this.start==1){
      onTimer();
    }


   
    this.sendOtp();
  }



  checkData(){
    if((this.formData.c1!='')&&(this.formData.c2!='')&&(this.formData.c3!='')&&(this.formData.c4!='')&&(this.formData.c5!='')){
      this.formInfo.otp = this.formData.c1 + this.formData.c2 + this.formData.c3 + this.formData.c4 + this.formData.c5;    
      this.validateOTP();
    }
  }

  validateOTP(){
    $(".loading").show();
    $(".resendOTP").hide();
    var data = JSON.stringify(this.formInfo);
    this.api_service.validate_otp(data).subscribe(res=>{
      this.resp = res;
      if(this.resp.body.hostHeaderInfo.responseCode === "000"){
        this.cookieService.set( 'Cookie', res.headers.get('Cookie'));
        this.router.navigate(['/user-validation']);
      }else{
        $(".loading").hide();
        $(".resendOTP").show();
        toast(this.resp.body.hostHeaderInfo.responseMessage,3000);
      }
    },error=>{
      $(".resendOTP").show();
      $(".loading").hide();
      toast("Oops Try again later",3000);
    });
  }

  back(){
    this.router.navigate(['/welcome']);
  }
  sendOtp(){

    $(".loading").show();
    $(".resendOTP").hide();
    
    this.otpObj.accountNumber = this.storage_service.getAcc();
      var data = JSON.stringify(this.otpObj);
      this.api_service.send_otp(data).subscribe(res=>{
        this.response = res;
        if(this.response.hostHeaderInfo.responseCode === "000"){
          $(".loading").hide();
          $(".resendOTP").show();
          toast("OTP resent successfully!",3000); 
        }

        else{
          $(".loading").hide();
          $(".resendOTP").show();
          toast(this.response.body.hostHeaderInfo.responseMessage,3000);
        }
      },error=>{
        $(".loading").hide();
        $(".resendOTP").show();
        toast("Oops Please try again later",3000);
      });
  }
  

}
