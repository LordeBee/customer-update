import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class StorageServiceService {
  

  constructor(private cookieService: CookieService) { }


  loggedIn(){
    return this.cookieService.get("Cookie");
  }

  userData(){
    return this.cookieService.get("User");
  }

  userPhone(){
    return this.cookieService.get("phone");
  }

  endSession(){
    this.cookieService.delete('Cookie');
  }

  getAcc(){
    return this.cookieService.get("Account");
  }


  saveInfo(key:string, info:string) {
    localStorage.setItem(key, info);
  }

  getInfo(key: string) {
    return localStorage.getItem(key);
  }

  clearInfo(key:string) {
    localStorage.removeItem(key);
  }

  

}