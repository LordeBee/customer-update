import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  private baseUrl = "https://196.8.204.71:8002/tin/api/";
  // private baseUrl = "/api/";


    private sendOtpUrl = this.baseUrl + "generateotp";
    private verifyTIN = this.baseUrl +  "verifyTin";
    private updateTIN = this.baseUrl +  "tinupdate";
    private verifyOTPUrl = this.baseUrl + "validateotp";

  constructor(private _http:HttpClient) { }



  verify_tin(data, token){
    let params = new URLSearchParams();
    params.set('tin', data);
    return this._http.post(this.verifyTIN, params.toString() , {
      headers: new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Access-Control-Allow-Origin', '*')
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      .set("authorization", token)
    }); 
  }


  update_tin(data, token){
    return this._http.post(this.updateTIN, data, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set("authorization", token)
    });
  }


  validate_otp(data){
    return this._http.post(this.verifyOTPUrl, data, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json'),
        observe: 'response'
    });
  }



   send_otp(data){
    return this._http.post(this.sendOtpUrl, data, {
        headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
    });
  }
 

 

 
}
