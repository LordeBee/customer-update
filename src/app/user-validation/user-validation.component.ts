import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../service/api-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageServiceService } from '../service/storage-service.service';
import { toast } from 'angular2-materialize';

declare var $:any;


@Component({
  selector: 'app-user-validation',
  templateUrl: './user-validation.component.html',
  styleUrls: ['./user-validation.component.css']
})
export class UserValidationComponent implements OnInit {

  formData:any;
  resp:any;
  resp3:any;
  response:any;
  response1:any;
  cif:any;
  cifId:any;
  loading=false;
  custName:any;
  getInfo:any;
  formInfo:any;
  loader=false;
  


  constructor(private api_service:ApiServiceService,private route: ActivatedRoute, private router: Router, private storage_service: StorageServiceService) { 
    if (!this.formData) {
      this.formData ={
        hostHeaderInfo:{
        requestId:"1",
        ipAddress:"0.0.0.0",
        sourceChannelId:"WEB"
        },
        cif:'',
        tin:'',
        staffId:'',
        status:'',
        comments:''
      }
    } 
    
    if (!this.getInfo) {
      this.getInfo = {
        state:''
      }
      
    } 

    if (!this.formInfo) {
      this.formInfo = {
        cif:''
      }
      
    } 
  }

  ngOnInit() {

    $('.loader').hide();
    var key = 'userObj';
    var userInfo = JSON.parse(this.storage_service.getInfo(key));
    this.custName = userInfo.firstName + " " + userInfo.lastName;
    this.formData.cif = userInfo.cif;
    this.cif = userInfo.cif;
    //console.log(userInfo);

  



    $(function(){
      $('input[type="radio"]').click(function(){
        if ($(this).is(':checked'))
        {
          if ($(this).val() == '1') {
    
            $('.yes').removeClass('hide');
            $('.no').addClass('hide');
            $('.update').removeClass('hide');
            $('.exit').addClass('hide');
            $('.not_2').removeClass('hide');
            $('.not').addClass('hide');
            $('.content_question #not_2').css("color", "#939395"); 
    
    
          }
          else if ($(this).val() == '2') {
            $('.not_2').addClass('hide');
            $('.not').removeClass('hide');
            $('.yes').addClass('hide');
            $('.not1').addClass('hide');
            $('.not1_2').removeClass('hide');
            $('.content_question #not1_2').css("color", "#939395"); 
            $('.no').removeClass('hide');
            $('.yes').addClass('hide');
            $('.exit').removeClass('hide');
            $('.update').addClass('hide');
      
          }
            
          else if ($(this).val() == '3') {
            $('.content_question #not1_2').css("color", "#FFFFFF"); 
            $('.no').addClass('hide');
            $('.update').removeClass('hide');
            $('.exit').addClass('hide');
            $('.yes').removeClass('hide');
            $('.not').addClass('hide');
            $('.not_2').removeClass('hide');
            $('.content_question #not_2').css("color", "#939395");
    
         }
    
          else if ($(this).val() == '4') {
            $('.content_question #not_2').css("color", "#FFFFFF"); 
            $('.no').removeClass('hide');
            $('.exit').removeClass('hide');
            $('.update').addClass('hide');
            $('.yes').addClass('hide');
            $('.not1').addClass('hide');
            $('.not1_2').removeClass('hide');
            $('.content_question #not1_2').css("color", "#939395"); 
    
    
            
    
    
    
    
          }
        }
      });
    });
    
  }


  nameChecker(tinName:any,custName:any) : boolean{
    var resp = false;

    var nameVar = this.custName;
    var info = nameVar.split(" ");
    var count = info.length;

    for (var i = 0; i < count; i++){
      resp = tinName.includes(info[i]);

      if(resp==true){
        break;
      }
      else if((resp==false)&&(info[i].length!= count)){
        continue;
      }

    }

    return resp;
    
  }

  updateTin(){
   
    var tinData = this.formData.tin.toUpperCase();
      var tinLen = tinData.length;
      var tinFirstChar = tinData.substring(0, 1);


    if(this.formData.tin==''){
      toast("Please enter your TIN",3000);
    } 

    else if((tinLen!=11)&&(tinFirstChar!='P')){
      toast("Please enter a valid TIN",3000);
    }
    

    else if((tinLen==11)&&(tinFirstChar=='P')){
      $('.loader').show();
      $('.update').addClass('hide');
      this.formData.cif = this.cif;
      var data = JSON.stringify(this.formData);
      if(this.cif!=''){
        this.updateData();
      }

      else{
        var key1 = 'userObj';
        this.storage_service.clearInfo(key1);
        this.router.navigate(['/welcome']);
      }
     

      
    }

    else{
      toast("Please enter a valid TIN",3000);
    }
  }

  


  confirmRequest(){
    
    this.formData.status = 'TIN CREATION';
    var info = JSON.stringify(this.formData);

    if(this.cif!=''){
      this.loader = true;
      var Cookie = this.storage_service.loggedIn();
       this.api_service.update_tin(info,Cookie).subscribe(res=>{
      this.resp = res;
      if(this.resp.hostHeaderInfo.responseCode == "000"){
        var key = 'userObj';
        this.storage_service.clearInfo(key);
        this.storage_service.endSession();
        this.router.navigate(['/confirm']);
        

      }
      else{
        this.loader = false;
         this.router.navigate(['/error-page']);
      }
      }, error => {
        this.loader = false;
         this.router.navigate(['/error-page']);

    });
    }

    
   

    

  }

  updateData(){
    this.formData.status = 'COMPLETED';
    var data = JSON.stringify(this.formData);
    var Cookie = this.storage_service.loggedIn();
      this.api_service.update_tin(data,Cookie).subscribe(res=>{
      this.response = res;
      if(this.response.hostHeaderInfo.responseCode == "000"){
        
        var key = 'userObj';
        this.storage_service.clearInfo(key);
        this.storage_service.endSession();
        this.router.navigate(['/congrats']);
      }
      else{
        toast(this.response.hostHeaderInfo.responseMessage ,3000);
        $('.loader').hide();
        $('.update').removeClass('hide');
        

      }
      }, error => {
        $('.loader').hide();
        $('.update').removeClass('hide');
        this.router.navigate(['/error-page']);
        
    });
  }

 

}
