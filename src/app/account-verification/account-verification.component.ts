import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../service/api-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageServiceService } from '../service/storage-service.service';
import { CookieService } from 'ngx-cookie-service';
import { toast } from 'angular2-materialize';

declare var $:any;

@Component({
  selector: 'app-account-verification',
  templateUrl: './account-verification.component.html',
  styleUrls: ['./account-verification.component.css']
}) 
export class AccountVerificationComponent implements OnInit {

  phone:any;
  resp:any;
  loading=false;
  formInfo:any;
  custInfo:any;
  response:any;

  constructor(private cookieService: CookieService,private api_service:ApiServiceService,private route: ActivatedRoute, private router: Router, private storage_service: StorageServiceService) { 
    if (!this.formInfo) {
      this.formInfo = {
        hostHeaderInfo:{
          requestId:"1",
          ipAddress:"127.0.0.1",
          sourceChannelId:"web"
       },
      accountNumber:""
      }
      
    }

    if (!this.custInfo) {
      this.custInfo = {
        phone:'',
      }
      
    }
  }

  ngOnInit() {
    // this.custInfo.phone = "233241110741";
   
    
  }

  
  verify(){

    if(this.formInfo.accountNumber.length==13){
      this.sendOtp();
    }
    
    
    
  }
  sendOtp(){
    this.loading = true;
      var data = JSON.stringify(this.formInfo);
      this.api_service.send_otp(data).subscribe(res=>{
        this.resp = res;
        this.loading = false;
        console.log(this.resp);

        if(this.resp.hostHeaderInfo.responseCode === "000"){
          this.cookieService.set( 'phone', this.custInfo.phone);
          this.cookieService.set( 'Account', this.formInfo.accountNumber);
          this.storage_service.saveInfo('userObj', JSON.stringify(this.resp));
          this.router.navigate(['/otp-verify']);
        }

        else{
          toast(this.resp.hostHeaderInfo.responseMessage,3000);
        }
      },error=>{
        this.loading = false;
        toast("Oops Please try again later");
      });
  }


  



  

}
