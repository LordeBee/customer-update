import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { ApiServiceService } from './service/api-service.service';
import  'materialize-css';
import {MaterializeModule} from 'angular2-materialize';
import { NgxMaskModule } from 'ngx-mask';
import { CookieService } from 'ngx-cookie-service';
import { AuthGuard } from './auth/auth.guard';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountVerificationComponent } from './account-verification/account-verification.component';
import { OtpVerifyComponent } from './otp-verify/otp-verify.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { UserValidationComponent } from './user-validation/user-validation.component';
import { TinValidationComponent } from './tin-validation/tin-validation.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { CongratsComponent } from './congrats/congrats.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountVerificationComponent,
    OtpVerifyComponent,
    WelcomeComponent,
    UserValidationComponent,
    TinValidationComponent,
    ConfirmComponent,
    ErrorPageComponent,
    CongratsComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MaterializeModule,
    NgxMaskModule.forRoot(),
    AppRoutingModule
  ],
  providers: [AuthGuard,CookieService,ApiServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
