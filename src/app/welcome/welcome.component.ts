import { Component, OnInit } from '@angular/core';
import { StorageServiceService } from '../service/storage-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css'] 
})
export class WelcomeComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router,private storage_service: StorageServiceService) { }

  ngOnInit() {
  }

  move(){
    var key = 'custObj';
    this.router.navigate(['/account-verification']);
  }

}
