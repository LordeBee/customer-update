import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountVerificationComponent } from './account-verification/account-verification.component';
import { OtpVerifyComponent } from './otp-verify/otp-verify.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { CongratsComponent } from './congrats/congrats.component';
import { UserValidationComponent } from './user-validation/user-validation.component';
import { TinValidationComponent } from './tin-validation/tin-validation.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AuthGuard } from './auth/auth.guard';


const routes: Routes = [
  { path: "account-verification", component: AccountVerificationComponent },
  { path: "otp-verify", component: OtpVerifyComponent },
  { path: "user-validation", component: UserValidationComponent,canActivate: [AuthGuard] },
  { path: "tin-validation", component: TinValidationComponent,canActivate: [AuthGuard] },
  { path: "error-page", component: ErrorPageComponent },
  { path: "confirm", component: ConfirmComponent },
  { path: "congrats", component: CongratsComponent },
  { path: "welcome", component: WelcomeComponent },
  {path: "", redirectTo: "/welcome",pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
}) 
export class AppRoutingModule { }
